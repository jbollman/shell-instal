
#!/bin/bash

code --install-extension vscoss.vscode-ansible //ansible
code --install-extension johnpapa.Angular2 //Angular FE Dev
code --install-extension timonwong.shellcheck //bash
code --install-extension ms-dotnettools.csharp //csharp
code --install-extension ms-azuretools.vscode-docker //docker
code --install-extension eriklynd.json-tools //json
code --install-extension ms-kubernetes-tools.vscode-kubernetes-tools //kubernetes
code --install-extension ms-vscode.powershell //powershell
code --install-extension ms-python.python //python
code --install-extension josefpihrt-vscode.roslynator //roslyn c# anaylyzer
code --install-extension hashicorp.terraform //terraform
code --install-extension redhat.vscode-yaml //yaml
